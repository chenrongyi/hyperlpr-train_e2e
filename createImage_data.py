import numpy as np
import cv2
import pickle


# 增加样本batchsize大小 by chen
batchSize = 5

# create train label
a = np.loadtxt('label.txt')
b = np.zeros([batchSize, 65])
for i in range(batchSize):
    for j in range(7):
        b[i, int(a[i, j])] = int(a[i, j])
print(b[0, :])

# create image train data
# 修改训练数据路径 by chen
path = 'G:/git_temp/hyperlpr-train_e2e/data/train_data'
img_data = np.zeros([batchSize, 30, 120, 3])
for i in range(batchSize):
    img_path = path + '/' + str(i).zfill(5) + ".jpg"
    b = cv2.imread(img_path)
    img_data[i, :, :, :] = b
    print('num:' + str(i))
    np.save('img_data.npy', img_data)

'''
output = open('img_data.pkl', 'wb')
pickle.dump(img_data, output)
'''
'''
img_path = path + '/' + str(0).zfill(5) + ".jpg"
b = cv2.imread(img_path)
a = np.load('pp.npy')
print(a[0,:,:,:] == b)
'''